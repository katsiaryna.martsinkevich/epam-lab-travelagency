package com.epam.katermar.travelagency.util;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;

/**
 * Created by katermar on 3/31/2018.
 */
public class ContextLoader {
    private static final String CONTEXT_PATH = "/spring/spring.xml";

    public static ApplicationContext loadContextByProfiles(String... profiles) {
        GenericXmlApplicationContext context = new GenericXmlApplicationContext();
        context.getEnvironment().setActiveProfiles(profiles);
        context.load(CONTEXT_PATH);
        context.refresh();

        return context;
    }
}
