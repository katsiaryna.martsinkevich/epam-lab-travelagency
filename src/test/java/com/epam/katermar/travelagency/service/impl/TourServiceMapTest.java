package com.epam.katermar.travelagency.service.impl;

import com.epam.katermar.travelagency.domain.Country;
import com.epam.katermar.travelagency.domain.Hotel;
import com.epam.katermar.travelagency.domain.Tour;
import com.epam.katermar.travelagency.service.TourService;
import com.epam.katermar.travelagency.util.ContextLoader;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Period;

public class TourServiceMapTest {
    private TourService tourService;
    private Tour firstTour;
    private Tour secondTour;

    @Before
    public void setUp() throws Exception {
        ApplicationContext context = ContextLoader.loadContextByProfiles("map");
        tourService = context.getBean(TourService.class);

        tourService.deleteAll();
        Country belarus = new Country(1, "Belarus");
        Hotel hotel = new Hotel("hotel", "80172443423", belarus, 5);
        Tour.TourType tourType = Tour.TourType.EDUCATION;
        firstTour = new Tour.Builder()
                .id(1)
                .photo("photo")
                .date(LocalDate.now())
                .duration(Period.ofDays(3))
                .country(belarus)
                .hotel(hotel)
                .type(tourType)
                .description("no")
                .cost(BigDecimal.valueOf(300))
                .build();
        secondTour = new Tour.Builder()
                .id(2)
                .photo("photo")
                .date(LocalDate.now())
                .duration(Period.ofDays(10))
                .country(belarus)
                .hotel(hotel)
                .type(tourType)
                .description("no")
                .cost(BigDecimal.valueOf(300))
                .build();
    }

    @After
    public void tearDown() throws Exception {
        tourService.deleteAll();
    }

    @Test
    public void save() {
        Assert.assertEquals(tourService.save(secondTour), secondTour);
    }

    @Test
    public void findById() {
        tourService.save(firstTour);
        Assert.assertEquals(tourService.findById(1), firstTour);
    }

    @Test
    public void findAll() {
        tourService.save(firstTour);
        tourService.save(secondTour);
        Assert.assertTrue(tourService.findAll().size() == 2);
    }
}