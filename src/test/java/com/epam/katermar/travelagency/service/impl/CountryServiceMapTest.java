package com.epam.katermar.travelagency.service.impl;

import com.epam.katermar.travelagency.domain.Country;
import com.epam.katermar.travelagency.service.CountryService;
import com.epam.katermar.travelagency.util.ContextLoader;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;

import java.util.ArrayList;
import java.util.List;

public class CountryServiceMapTest {
    private CountryService countryService;
    private Country belarus;
    private Country usa;
    private Country poland;
    private List<Country> countries;

    @Before
    public void setUp() throws Exception {
        ApplicationContext context = ContextLoader.loadContextByProfiles("map");
        countryService = context.getBean(CountryService.class);

        belarus = new Country(1, "Belarus");
        usa = new Country(2, "USA");
        poland = new Country(3, "Poland");

        countries = new ArrayList<>();
        countries.add(usa);
        countries.add(poland);

        countries.forEach(countryService::save);
    }

    @After
    public void tearDown() throws Exception {
        countryService.deleteAll();
    }

    @Test
    public void save() throws Exception {
        Assert.assertEquals(countryService.save(belarus), belarus);
        countryService.deleteById(belarus.getId());
    }

    @Test
    public void findById() {
        Assert.assertEquals(countryService.findById(usa.getId()), usa);

    }

    @Test
    public void count() {
        Assert.assertEquals(3, countryService.findAll().size());
    }

    @Test
    public void deleteById() {
        countryService.deleteById(poland.getId());
    }

    @Test
    public void delete() {
        countryService.deleteById(belarus.getId());
    }

    @Test
    public void findAll() {
        Assert.assertEquals(3, countryService.findAll().size());
    }
}