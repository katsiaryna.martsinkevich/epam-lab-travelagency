package com.epam.katermar.travelagency.service.impl;

import com.epam.katermar.travelagency.exception.NoDataException;
import com.epam.katermar.travelagency.service.CountryService;
import com.epam.katermar.travelagency.util.ContextLoader;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class CountryServiceDBTest {
    private CountryService countryService;

    @Before
    public void setUp() throws Exception {
        ApplicationContext context = ContextLoader.loadContextByProfiles("dev", "db");
        countryService = context.getBean(CountryService.class);
    }

    @Test
    public void findById() throws Exception {
        assertEquals("Belarus", countryService.findById(1).getName());
    }

    @Test(expected = NoDataException.class)
    public void findByNonExistentId() throws Exception {
        countryService.findById(8);
    }

    @Test
    public void findAll() throws Exception {
        assertTrue(countryService.findAll().size() >= 6);
    }
}
