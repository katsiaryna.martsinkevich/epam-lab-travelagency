package com.epam.katermar.travelagency.service.impl;

import com.epam.katermar.travelagency.domain.Country;
import com.epam.katermar.travelagency.service.CountryService;
import com.epam.katermar.travelagency.util.ContextLoader;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by katermar on 3/31/2018.
 */
public class CountryServiceEmbeddedDBTest {
    private CountryService countryService;

    @Before
    public void setUp() throws Exception {
        ApplicationContext context = ContextLoader.loadContextByProfiles("test", "db");
        countryService = context.getBean(CountryService.class);
    }

    @Test
    public void save() throws Exception {
        Country poland = countryService.save(new Country(1, "Poland"));
        assertEquals(countryService.findById(poland.getId()), poland);
    }

    @Test
    public void findById() throws Exception {
        assertEquals("Belarus", countryService.findById(1).getName());
    }

    @Test
    public void findAll() throws Exception {
        assertTrue(countryService.findAll().size() >= 6);
    }

    @Test
    public void deleteById() throws Exception {
        countryService.deleteById(3);
    }
}