package com.epam.katermar.travelagency.service.impl;

import com.epam.katermar.travelagency.service.TourService;
import com.epam.katermar.travelagency.util.ContextLoader;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;

import java.time.LocalDate;
import java.time.Period;

import static org.junit.Assert.assertTrue;

public class TourServiceEmbeddedDBTest {

    private TourService repository;

    @Before
    public void init() {
        ApplicationContext context = ContextLoader.loadContextByProfiles("test", "db");
        repository = context.getBean(TourService.class);
    }

    @Test
    public void findByIdParseDuration() {
        assertTrue(repository.findById(1).getDuration().equals(Period.ofDays(7)));
    }

    @Test
    public void findByIdParseDate() {
        assertTrue(repository.findById(1).getDate().equals(LocalDate.parse("2018-05-17")));
    }
}