package com.epam.katermar.travelagency.repository.db.impl;

import com.epam.katermar.travelagency.domain.Country;
import com.epam.katermar.travelagency.repository.CrudRepository;
import com.epam.katermar.travelagency.util.ContextLoader;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import static org.junit.Assert.assertEquals;

public class CountryDBRepositoryTest {
    private NamedParameterJdbcTemplate jdbcTemplate;
    private CrudRepository<Country> countryDAORepository;

    @Before
    public void init() {
        ApplicationContext context = ContextLoader.loadContextByProfiles("test", "db");
        countryDAORepository = (CountryDBRepository) context.getBean("countryRepository");
        jdbcTemplate = context.getBean(NamedParameterJdbcTemplate.class);
    }

    @Test
    public void test() {
        assertEquals(countryDAORepository.findById(1).orElse(new Country()), new Country(1, "Belarus"));
    }

    @Test
    public void testDataAccess() {
        assertEquals(1, jdbcTemplate
                .query("SELECT count(id) FROM country WHERE id = 1",
                        (resultSet, row) -> resultSet.getInt(1)).size());
    }
}
