package com.epam.katermar.travelagency.repository.impl;

import com.epam.katermar.travelagency.domain.Country;
import com.epam.katermar.travelagency.repository.map.impl.CountryRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;
import java.util.Map;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class CountryRepositoryTest {
    @InjectMocks
    private CountryRepository countryRepository;
    @Mock
    private List<Country> countries;
    @Mock
    private Map<Integer, Country> countryMap;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testTourTypeExists() throws Exception {
        Country country = new Country(1, "country");
        country.setId(1);
        when(countryRepository.existsById(country.getId())).thenReturn(true);
        Assert.assertTrue(countryRepository.existsById(country.getId()));
    }

    @Test
    public void testCounter() throws Exception {
        Country country = new Country(1, "country");
        when(countries.get(0)).thenReturn(country);
        Assert.assertEquals(countryRepository.save(country), countries.get(0));
    }

    @Test
    public void testRepositorySize() throws Exception {
        int repositorySize = 7;
        when(countryMap.size()).thenReturn(repositorySize);
        Assert.assertEquals(countryRepository.count(), repositorySize);
        verify(countryMap).size();
    }
}