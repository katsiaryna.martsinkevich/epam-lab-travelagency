package com.epam.katermar.travelagency.domain;

import java.util.Objects;

/**
 * Created by katermar on 3/17/2018.
 */
public abstract class IDEntity {
    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IDEntity idEntity = (IDEntity) o;
        return Objects.equals(id, idEntity.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
