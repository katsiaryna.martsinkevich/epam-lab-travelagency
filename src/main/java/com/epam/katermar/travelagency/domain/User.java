package com.epam.katermar.travelagency.domain;

import java.util.List;
import java.util.Objects;

/**
 * Created by katermar on 3/15/2018.
 */
public class User extends IDEntity {
    private String login;
    private String password;
    private List<Tour> tours;
    private List<Review> reviews;

    public User() {
    }

    public User(Builder builder) {
        this.setId(builder.id);
        this.login = builder.login;
        this.password = builder.password;
        this.tours = builder.tours;
        this.reviews = builder.reviews;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Tour> getTours() {
        return tours;
    }

    public void setTours(List<Tour> tours) {
        this.tours = tours;
    }

    public List<Review> getReviews() {
        return reviews;
    }

    public void setReviews(List<Review> reviews) {
        this.reviews = reviews;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(login, user.login) &&
                Objects.equals(password, user.password) &&
                Objects.equals(tours, user.tours) &&
                Objects.equals(reviews, user.reviews);
    }

    @Override
    public int hashCode() {

        return Objects.hash(login, password, tours, reviews);
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + getId() +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", tours=" + tours +
                ", reviews=" + reviews +
                '}';
    }

    public static class Builder {
        private Integer id;
        private String login;
        private String password;
        private List<Tour> tours;
        private List<Review> reviews;

        public Builder id(Integer id) {
            this.id = id;
            return this;
        }

        public Builder login(String login) {
            this.login = login;
            return this;
        }

        public Builder password(String password) {
            this.password = password;
            return this;
        }

        public Builder tours(List<Tour> tours) {
            this.tours = tours;
            return this;
        }

        public Builder reviews(List<Review> reviews) {
            this.reviews = reviews;
            return this;
        }

        public User build() {
            return new User(this);
        }
    }
}
