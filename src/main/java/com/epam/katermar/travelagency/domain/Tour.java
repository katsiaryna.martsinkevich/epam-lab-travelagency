package com.epam.katermar.travelagency.domain;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Period;
import java.util.Objects;

/**
 * Created by katermar on 3/15/2018.
 */
public class Tour extends IDEntity {
    private String photo;
    private LocalDate date;
    private Period duration;
    private Country country;
    private Hotel hotel;
    private TourType type;
    private String description;
    private BigDecimal cost;

    public Tour() {
    }

    private Tour(Builder builder) {
        this.setId(builder.id);
        this.photo = builder.photo;
        this.date = builder.date;
        this.duration = builder.duration;
        this.country = builder.country;
        this.hotel = builder.hotel;
        this.type = builder.type;
        this.description = builder.description;
        this.cost = builder.cost;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Period getDuration() {
        return duration;
    }

    public void setDuration(Period duration) {
        this.duration = duration;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public Hotel getHotel() {
        return hotel;
    }

    public void setHotel(Hotel hotel) {
        this.hotel = hotel;
    }

    public TourType getType() {
        return type;
    }

    public void setType(TourType type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tour tour = (Tour) o;
        return Objects.equals(photo, tour.photo) &&
                Objects.equals(date, tour.date) &&
                Objects.equals(duration, tour.duration) &&
                Objects.equals(country, tour.country) &&
                Objects.equals(hotel, tour.hotel) &&
                Objects.equals(type, tour.type) &&
                Objects.equals(description, tour.description) &&
                Objects.equals(cost, tour.cost);
    }

    @Override
    public int hashCode() {
        return Objects.hash(photo, date, duration, country, hotel, type, description, cost);
    }

    @Override
    public String toString() {
        return "Tour{" +
                "id=" + getId() +
                ", photo='" + photo + '\'' +
                ", date=" + date +
                ", duration=" + duration +
                ", country=" + country +
                ", hotel=" + hotel +
                ", type=" + type +
                ", description='" + description + '\'' +
                ", cost=" + cost +
                '}';
    }

    public enum TourType {
        TRAVEL, EDUCATION
    }

    public static class Builder {
        private Integer id;
        private String photo;
        private LocalDate date;
        private Period duration;
        private Country country;
        private Hotel hotel;
        private TourType type;
        private String description;
        private BigDecimal cost;

        public Builder id(Integer id) {
            this.id = id;
            return this;
        }

        public Builder photo(String photo) {
            this.photo = photo;
            return this;
        }

        public Builder date(LocalDate date) {
            this.date = date;
            return this;
        }

        public Builder duration(Period duration) {
            this.duration = duration;
            return this;
        }

        public Builder country(Country country) {
            this.country = country;
            return this;
        }

        public Builder hotel(Hotel hotel) {
            this.hotel = hotel;
            return this;
        }

        public Builder type(TourType type) {
            this.type = type;
            return this;
        }

        public Builder description(String description) {
            this.description = description;
            return this;
        }

        public Builder cost(BigDecimal cost) {
            this.cost = cost;
            return this;
        }

        public Tour build() {
            return new Tour(this);
        }
    }
}
