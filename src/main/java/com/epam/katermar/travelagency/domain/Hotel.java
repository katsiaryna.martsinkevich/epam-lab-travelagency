package com.epam.katermar.travelagency.domain;

import java.util.Objects;

/**
 * Created by katermar on 3/15/2018.
 */
public class Hotel extends IDEntity {
    private String name;
    private String phone;
    private Country country;
    private Integer stars;

    public Hotel() {
    }

    public Hotel(String name, String phone, Country country, Integer stars) {
        this.name = name;
        this.phone = phone;
        this.country = country;
        this.stars = stars;
    }

    private Hotel(Builder builder) {
        this.setId(builder.id);
        this.name = builder.name;
        this.phone = builder.phone;
        this.country = builder.country;
        this.stars = builder.stars;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public Integer getStars() {
        return stars;
    }

    public void setStars(Integer stars) {
        this.stars = stars;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Hotel hotel = (Hotel) o;
        return Objects.equals(name, hotel.name) &&
                Objects.equals(phone, hotel.phone) &&
                Objects.equals(country, hotel.country) &&
                Objects.equals(stars, hotel.stars);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, phone, country, stars);
    }

    @Override
    public String toString() {
        return "Hotel{" +
                "id=" + getId() +
                ", name='" + name + '\'' +
                ", phone='" + phone + '\'' +
                ", country=" + country +
                ", stars=" + stars +
                '}';
    }

    public static class Builder {
        private Integer id;
        private String name;
        private String phone;
        private Country country;
        private Integer stars;

        public Builder id(Integer id) {
            this.id = id;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder phone(String phone) {
            this.phone = phone;
            return this;
        }

        public Builder country(Country country) {
            this.country = country;
            return this;
        }

        public Builder stars(Integer stars) {
            this.stars = stars;
            return this;
        }

        public Hotel build() {
            return new Hotel(this);
        }
    }
}
