package com.epam.katermar.travelagency.domain;

import java.util.Objects;

/**
 * Created by katermar on 3/15/2018.
 */
public class Review extends IDEntity {
    private Tour tour;
    private User user;
    private String content;

    public Review() {
    }

    private Review(Builder builder) {
        this.setId(builder.id);
        this.tour = builder.tour;
        this.user = builder.user;
        this.content = builder.content;
    }

    public Tour getTour() {
        return tour;
    }

    public void setTour(Tour tour) {
        this.tour = tour;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Review review = (Review) o;
        return Objects.equals(tour, review.tour) &&
                Objects.equals(user, review.user) &&
                Objects.equals(content, review.content);
    }

    @Override
    public int hashCode() {
        return Objects.hash(tour, user, content);
    }

    @Override
    public String toString() {
        return "Review{" +
                "id=" + getId() +
                ", tour=" + tour +
                ", user=" + user +
                ", content='" + content + '\'' +
                '}';
    }

    public static class Builder {
        private Integer id;
        private Tour tour;
        private User user;
        private String content;

        public Builder id(Integer id) {
            this.id = id;
            return this;
        }

        public Builder tour(Tour tour) {
            this.tour = tour;
            return this;
        }

        public Builder user(User user) {
            this.user = user;
            return this;
        }

        public Builder content(String content) {
            this.content = content;
            return this;
        }

        public Review build() {
            return new Review(this);
        }
    }
}
