package com.epam.katermar.travelagency.exception;

public class NoDataException extends RuntimeException {
    public NoDataException() {
        super("No data was found");
    }
}
