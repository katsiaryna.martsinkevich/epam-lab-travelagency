package com.epam.katermar.travelagency.repository.map.impl;

import com.epam.katermar.travelagency.domain.Hotel;
import com.epam.katermar.travelagency.repository.map.AbstractCrudRepository;

public class HotelRepository extends AbstractCrudRepository<Hotel> {
}
