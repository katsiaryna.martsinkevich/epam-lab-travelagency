package com.epam.katermar.travelagency.repository.db.impl;

import com.epam.katermar.travelagency.domain.Country;
import com.epam.katermar.travelagency.domain.Hotel;
import com.epam.katermar.travelagency.exception.NoDataException;
import com.epam.katermar.travelagency.repository.CrudRepository;
import com.epam.katermar.travelagency.repository.db.AbstractDBRepository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public class HotelDBRepository extends AbstractDBRepository<Hotel> implements CrudRepository<Hotel> {
    private static final String INSERT_INTO_HOTEL_NAME_PHONE_ID_COUNTRY_STARS_VALUES = "INSERT INTO hotel (name, phone, id_country, stars)" +
            " VALUES (:name, :phone, :country.id, :stars)";
    private static final String SELECT_FROM_HOTEL_WHERE_ID = "SELECT id, name, phone, id_country, stars FROM hotel WHERE id = :id ";
    private static final String DELETE_FROM_HOTEL_WHERE_ID = "DELETE FROM hotel WHERE id = :id";
    private static final String SELECT_FROM_HOTEL = "SELECT id, name, phone, id_country, stars FROM hotel";
    private static final String DELETE_FROM_HOTEL = "DELETE FROM hotel";

    private CrudRepository<Country> countryRepository;

    @Override
    public <S extends Hotel> S save(S entity) {
        return save(entity, INSERT_INTO_HOTEL_NAME_PHONE_ID_COUNTRY_STARS_VALUES);
    }

    @Override
    public Optional<Hotel> findById(Integer id) {
        return findById(id, SELECT_FROM_HOTEL_WHERE_ID, (resultSet, rowNum) -> Optional.ofNullable(mapRow(resultSet, rowNum)));
    }

    @Override
    public List<Hotel> findAll() {
        return findAll(SELECT_FROM_HOTEL, this::mapRow);
    }

    @Override
    public void deleteById(Integer id) {
        deleteById(id, DELETE_FROM_HOTEL_WHERE_ID);
    }

    @Override
    public void deleteAll() {
        deleteAll(DELETE_FROM_HOTEL);
    }

    private Hotel mapRow(ResultSet resultSet, int rowNum) throws SQLException {
        return new Hotel.Builder()
                .country(countryRepository.findById(resultSet.getInt("id_country")).orElseThrow(NoDataException::new))
                .name(resultSet.getString("name"))
                .phone(resultSet.getString("phone"))
                .stars(resultSet.getInt("stars"))
                .build();
    }

    public void setCountryRepository(CrudRepository<Country> countryRepository) {
        this.countryRepository = countryRepository;
    }
}
