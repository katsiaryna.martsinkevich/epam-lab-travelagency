package com.epam.katermar.travelagency.repository.map.impl;

import com.epam.katermar.travelagency.domain.Review;
import com.epam.katermar.travelagency.repository.map.AbstractCrudRepository;

public class ReviewRepository extends AbstractCrudRepository<Review> {
}
