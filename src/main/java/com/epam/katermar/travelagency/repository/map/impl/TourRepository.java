package com.epam.katermar.travelagency.repository.map.impl;

import com.epam.katermar.travelagency.domain.Tour;
import com.epam.katermar.travelagency.repository.map.AbstractCrudRepository;

public class TourRepository extends AbstractCrudRepository<Tour> {
}
