package com.epam.katermar.travelagency.repository.db.impl;

import com.epam.katermar.travelagency.domain.User;
import com.epam.katermar.travelagency.repository.CrudRepository;
import com.epam.katermar.travelagency.repository.db.AbstractDBRepository;
import com.epam.katermar.travelagency.repository.db.ReviewDBRepository;
import com.epam.katermar.travelagency.repository.db.TourDBRepository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public class UserDBRepository extends AbstractDBRepository<User> implements CrudRepository<User> {
    private static final String INSERT_INTO_REVIEW_CONTENT_ID_USER_ID_TOUR_VALUES = "INSERT INTO \"user\" (login, password) VALUES (:login, :password)";
    private static final String SELECT_FROM_HOTEL_WHERE_ID = "SELECT id, login, password FROM \"user\" WHERE id = :id ";
    private static final String DELETE_FROM_HOTEL_WHERE_ID = "DELETE FROM \"user\" WHERE id = :id";
    private static final String SELECT_FROM_HOTEL = "SELECT id, login, password FROM \"user\"";
    private static final String DELETE_FROM_HOTEL = "DELETE FROM \"user\"";

    private ReviewDBRepository reviewDBRepository;
    private TourDBRepository tourDBRepository;

    @Override
    public <S extends User> S save(S entity) {
        return save(entity, INSERT_INTO_REVIEW_CONTENT_ID_USER_ID_TOUR_VALUES);
    }

    @Override
    public Optional<User> findById(Integer id) {
        return findById(id, SELECT_FROM_HOTEL_WHERE_ID,
                (resultSet, i) -> Optional.ofNullable(mapRow(resultSet, i)));
    }

    @Override
    public List<User> findAll() {
        return findAll(SELECT_FROM_HOTEL, this::mapRow);
    }

    @Override
    public void deleteById(Integer id) {
        deleteById(id, DELETE_FROM_HOTEL_WHERE_ID);
    }

    @Override
    public void deleteAll() {
        deleteAll(DELETE_FROM_HOTEL);
    }

    private User mapRow(ResultSet resultSet, int rowNum) throws SQLException {
        int userId = resultSet.getInt("id");
        return new User.Builder()
                .id(userId)
                .login(resultSet.getString("login"))
                .password(resultSet.getString("password"))
                .reviews(reviewDBRepository.findByUserId(userId))
                .tours(tourDBRepository.findByUserId(userId))
                .build();
    }

    public void setReviewDBRepository(ReviewDBRepository reviewDBRepository) {
        this.reviewDBRepository = reviewDBRepository;
    }

    public void setTourDBRepository(TourDBRepository tourDBRepository) {
        this.tourDBRepository = tourDBRepository;
    }
}
