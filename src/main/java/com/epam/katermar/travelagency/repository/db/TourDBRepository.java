package com.epam.katermar.travelagency.repository.db;

import com.epam.katermar.travelagency.domain.Tour;
import com.epam.katermar.travelagency.repository.CrudRepository;

import java.util.List;

public interface TourDBRepository extends CrudRepository<Tour> {
    List<Tour> findByUserId(Integer id);
}
