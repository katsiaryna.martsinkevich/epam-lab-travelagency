package com.epam.katermar.travelagency.repository.map.impl;

import com.epam.katermar.travelagency.domain.User;
import com.epam.katermar.travelagency.repository.map.AbstractCrudRepository;

public class UserRepository extends AbstractCrudRepository<User> {
}
