package com.epam.katermar.travelagency.repository.map;

import com.epam.katermar.travelagency.domain.IDEntity;
import com.epam.katermar.travelagency.repository.CrudRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by katermar on 3/17/2018.
 */
public abstract class AbstractCrudRepository<T extends IDEntity> implements CrudRepository<T> {
    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractCrudRepository.class);

    private Map<Integer, T> entityMap = new HashMap<>();

    public AbstractCrudRepository() {
    }

    public AbstractCrudRepository(Iterable<T> entities) {
        entities.forEach(entity -> entityMap.put(entity.getId(), entity));
    }

    public void setEntityMap(Map<Integer, T> entityMap) {
        this.entityMap = entityMap;
    }

    @Override
    public <S extends T> S save(S entity) {
        Objects.requireNonNull(entity);
        LOGGER.debug("saving entity {} on a Repository layer.", entity);
        entityMap.putIfAbsent(entity.getId(), entity);
        return entity;
    }

    public <S extends T> Iterable<S> saveAll(List<S> entities) {
        Map<Integer, S> map = entities
                .stream()
                .collect(Collectors.toMap(S::getId, Function.identity()));
        LOGGER.debug("saving entities on a Repository layer.");
        entityMap.putAll(map);
        return map.values();
    }

    @Override
    public Optional<T> findById(Integer id) {
        Objects.requireNonNull(id);
        LOGGER.debug("finding entity by id {} on a Repository layer.", id);
        return Optional.ofNullable(entityMap.get(id));
    }

    public boolean existsById(Integer id) {
        Objects.requireNonNull(id);
        LOGGER.debug("defining existence of an entity with id {} on a Repository layer.", id);
        return entityMap.containsKey(id);
    }

    @Override
    public List<T> findAll() {
        LOGGER.debug("finding all on a Repository layer.");
        return new ArrayList<>(entityMap.values());
    }

    public Iterable<T> findAllById(Iterable<Integer> ids) {
        LOGGER.debug("finding all on a Repository layer.");
        List<T> foundEntities = new ArrayList<>();
        for (Integer id : ids) {
            Objects.requireNonNull(id);
            if (entityMap.containsKey(id)) {
                T entity = entityMap.get(id);
                LOGGER.debug("found entity {} on a Repository layer.", entity);
                foundEntities.add(entity);
            }
        }
        LOGGER.debug("found {} entities all on a Repository layer.", foundEntities.size());
        return foundEntities;
    }

    public long count() {
        LOGGER.debug("counting all on a Repository layer.");
        return entityMap.size();
    }

    @Override
    public void deleteById(Integer id) {
        Objects.requireNonNull(id);
        LOGGER.debug("deleting by id {} on a Repository layer.", id);
        if (entityMap.containsKey(id)) {
            entityMap.remove(id);
            LOGGER.debug("entity with id {} removed.", id);
        } else {
            LOGGER.debug("entity with id {} don't exist.", id);
        }
    }

    public void delete(T entity) {
        Objects.requireNonNull(entity);
        LOGGER.debug("deleting entity {} on a Repository layer.", entity);
        if (entityMap.containsValue(entity)) {
            entityMap.remove(entity.getId());
            LOGGER.debug("entity {} removed.", entity);
        } else {
            LOGGER.debug("entity {} don't exist.", entity);
        }
    }

    public void deleteAll(Iterable<? extends T> entities) {
        Objects.requireNonNull(entities);
        LOGGER.debug("deleting entities on a Repository layer.");
        entities.forEach(this::delete);
        LOGGER.debug("entities deleted on a Repository layer.");
    }

    @Override
    public void deleteAll() {
        entityMap.clear();
        LOGGER.debug("deleted all on a Repository layer.");
    }
}
