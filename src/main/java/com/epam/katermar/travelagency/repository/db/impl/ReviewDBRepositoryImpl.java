package com.epam.katermar.travelagency.repository.db.impl;

import com.epam.katermar.travelagency.domain.Review;
import com.epam.katermar.travelagency.domain.Tour;
import com.epam.katermar.travelagency.domain.User;
import com.epam.katermar.travelagency.exception.NoDataException;
import com.epam.katermar.travelagency.repository.CrudRepository;
import com.epam.katermar.travelagency.repository.db.AbstractDBRepository;
import com.epam.katermar.travelagency.repository.db.ReviewDBRepository;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class ReviewDBRepositoryImpl extends AbstractDBRepository<Review> implements ReviewDBRepository {
    private static final String INSERT_INTO_REVIEW_CONTENT_ID_USER_ID_TOUR_VALUES = "INSERT INTO review " +
            "(content, id_user, id_tour) VALUES (:content, :user.id, :tour.id)";
    private static final String SELECT_FROM_REVIEW_WHERE_ID = "SELECT id, content, id_user, id_tour FROM review WHERE id = :id ";
    private static final String DELETE_FROM_REVIEW_WHERE_ID = "DELETE FROM review WHERE id = :id";
    private static final String SELECT_FROM_REVIEW = "SELECT id, content, id_user, id_tour FROM review";
    private static final String DELETE_FROM_REVIEW = "DELETE FROM review";
    private static final String SELECT_FROM_REVIEW_WHERE_ID_USER = "SELECT id, content, id_user, id_tour FROM review WHERE id_user = :user.id";

    private CrudRepository<Tour> tourRepository;
    private CrudRepository<User> userRepository;

    @Override
    public <S extends Review> S save(S entity) {
        return save(entity, INSERT_INTO_REVIEW_CONTENT_ID_USER_ID_TOUR_VALUES);
    }

    @Override
    public Optional<Review> findById(Integer id) {
        return findById(id, SELECT_FROM_REVIEW_WHERE_ID,
                (resultSet, i) -> Optional.ofNullable(mapRow(resultSet, i)));
    }

    @Override
    public List<Review> findByUserId(Integer id) {
        Objects.requireNonNull(id);
        try {
            return getJdbcTemplate().query(
                    SELECT_FROM_REVIEW_WHERE_ID_USER,
                    new MapSqlParameterSource("id_user", id),
                    this::mapRow);
        } catch (EmptyResultDataAccessException e) {
            return Collections.emptyList();
        }
    }

    @Override
    public List<Review> findAll() {
        return findAll(SELECT_FROM_REVIEW, this::mapRow);
    }

    @Override
    public void deleteById(Integer id) {
        deleteById(id, DELETE_FROM_REVIEW_WHERE_ID);
    }

    @Override
    public void deleteAll() {
        deleteAll(DELETE_FROM_REVIEW);
    }

    private Review mapRow(ResultSet resultSet, int rowNum) throws SQLException {
        int reviewId = resultSet.getInt("id");
        return new Review.Builder()
                .id(reviewId)
                .content(resultSet.getString("content"))
                .tour(tourRepository.findById(reviewId).orElseThrow(NoDataException::new))
                .user(userRepository.findById(reviewId).orElseThrow(NoDataException::new))
                .build();
    }

    public void setTourRepository(CrudRepository<Tour> tourRepository) {
        this.tourRepository = tourRepository;
    }

    public void setUserRepository(CrudRepository<User> userRepository) {
        this.userRepository = userRepository;
    }
}
