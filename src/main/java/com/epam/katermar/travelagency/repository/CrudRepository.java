package com.epam.katermar.travelagency.repository;

import java.util.List;
import java.util.Optional;

public interface CrudRepository<T> {
    /**
     * Saves a given entity. Use the returned instance for further operations as the save operation might have changed the
     * entity instance completely.
     *
     * @param entity must not be null.
     * @return the saved entity.
     */
    <S extends T> S save(S entity);

    /**
     * Retrieves an entity by its id.
     *
     * @param id must not be null.
     * @return the entity with the given id or Optional.empty() if none found
     */
    Optional<T> findById(Integer id);


    /**
     * Returns all instances of the type.
     *
     * @return all entities
     */
    List<T> findAll();

    /**
     * Deletes the entity with the given id.
     *
     * @param id must not be null.
     */
    void deleteById(Integer id);


    /**
     * Deletes all entities managed by the repository.
     */
    void deleteAll();
}
