package com.epam.katermar.travelagency.repository.db;

import com.epam.katermar.travelagency.domain.Review;
import com.epam.katermar.travelagency.repository.CrudRepository;

import java.util.List;

public interface ReviewDBRepository extends CrudRepository<Review> {
    List<Review> findByUserId(Integer id);
}
