package com.epam.katermar.travelagency.repository.db.impl;

import com.epam.katermar.travelagency.domain.Country;
import com.epam.katermar.travelagency.domain.Hotel;
import com.epam.katermar.travelagency.domain.Tour;
import com.epam.katermar.travelagency.exception.NoDataException;
import com.epam.katermar.travelagency.repository.CrudRepository;
import com.epam.katermar.travelagency.repository.db.AbstractDBRepository;
import com.epam.katermar.travelagency.repository.db.TourDBRepository;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Period;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class TourDBRepositoryImpl extends AbstractDBRepository<Tour> implements TourDBRepository {
    private static final String INSERT_INTO_TOUR = "INSERT INTO tour " +
            "(photo, date, duration, id_country, id_hotel, description, cost, id_user, tour_type) " +
            "VALUES (:photo, :date, :duration, :country.id, :hotel.id, :description, :cost, :user.id, :type)";
    private static final String SELECT_FROM_REVIEW_WHERE_REVIEW_ID = "SELECT id, photo, date, duration, id_country, id_hotel, description, cost, id_user, tour_type FROM tour WHERE id = :id ";
    private static final String DELETE_FROM_REVIEW_WHERE_ID = "DELETE FROM tour WHERE id = :id";
    private static final String SELECT_FROM_REVIEW = "SELECT id, photo, date, duration, id_country, id_hotel, description, cost, id_user, tour_type FROM tour";
    private static final String DELETE_FROM_REVIEW = "DELETE FROM tour";
    private static final String SELECT_FROM_TOUR_WHERE_ID_USER = "SELECT id, photo, date, duration, id_country, id_hotel, description, cost, id_user, tour_type FROM tour WHERE id_user = :user.id";

    private CrudRepository<Country> countryRepository;
    private CrudRepository<Hotel> hotelRepository;

    @Override
    public <S extends Tour> S save(S entity) {
        return save(entity, INSERT_INTO_TOUR);
    }

    @Override
    public Optional<Tour> findById(Integer id) {
        return findById(id, SELECT_FROM_REVIEW_WHERE_REVIEW_ID,
                (resultSet, i) -> Optional.ofNullable(mapRow(resultSet, i)));
    }

    @Override
    public List<Tour> findByUserId(Integer id) {
        Objects.requireNonNull(id);
        try {
            return getJdbcTemplate().query(
                    SELECT_FROM_TOUR_WHERE_ID_USER,
                    new MapSqlParameterSource("id_user", id),
                    this::mapRow);
        } catch (EmptyResultDataAccessException e) {
            return Collections.emptyList();
        }
    }

    @Override
    public List<Tour> findAll() {
        return findAll(SELECT_FROM_REVIEW, this::mapRow);
    }

    @Override
    public void deleteById(Integer id) {
        deleteById(id, DELETE_FROM_REVIEW_WHERE_ID);
    }

    @Override
    public void deleteAll() {
        deleteAll(DELETE_FROM_REVIEW);
    }

    private Tour mapRow(ResultSet resultSet, int rowNum) throws SQLException {
        return new Tour.Builder()
                .id(resultSet.getInt("id"))
                .cost(resultSet.getBigDecimal("cost"))
                .country(countryRepository.findById(resultSet.getInt("id_country")).orElseThrow(NoDataException::new))
                .date(resultSet.getDate("date").toLocalDate())
                .description(resultSet.getString("description"))
                .duration(Period.parse(resultSet.getString("duration")))
                .hotel(hotelRepository.findById(resultSet.getInt("id_hotel")).orElseThrow(NoDataException::new))
                .type(Tour.TourType.valueOf(resultSet.getString("tour_type").toUpperCase()))
                .photo(resultSet.getString("photo"))
                .build();
    }

    public void setCountryRepository(CrudRepository<Country> countryRepository) {
        this.countryRepository = countryRepository;
    }

    public void setHotelRepository(CrudRepository<Hotel> hotelRepository) {
        this.hotelRepository = hotelRepository;
    }
}
