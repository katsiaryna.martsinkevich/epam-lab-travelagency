package com.epam.katermar.travelagency.repository.db;

import com.epam.katermar.travelagency.domain.IDEntity;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public abstract class AbstractDBRepository<T extends IDEntity> {
    private NamedParameterJdbcTemplate jdbcTemplate;

    protected NamedParameterJdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    public void setJdbcTemplate(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public <S extends T> S save(S entity, String query) {
        Objects.requireNonNull(entity);
        GeneratedKeyHolder holder = new GeneratedKeyHolder();
        jdbcTemplate.update(query,
                new BeanPropertySqlParameterSource(entity), holder, new String[]{"ID"});
        entity.setId(holder.getKey().intValue());
        return entity;
    }

    public Optional<T> findById(Integer id, String query, RowMapper<Optional<T>> rowMapper) {
        Objects.requireNonNull(id);
        try {
            return jdbcTemplate.queryForObject(
                    query,
                    new MapSqlParameterSource("id", id),
                    rowMapper);
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    public List<T> findAll(String query, RowMapper<T> rowMapper) {
        try {
            return jdbcTemplate.query(query, rowMapper);
        } catch (EmptyResultDataAccessException e) {
            return Collections.emptyList();
        }
    }

    public void deleteById(Integer id, String query) {
        Objects.requireNonNull(id);
        jdbcTemplate.update(query, new MapSqlParameterSource("id", id));
    }

    public void deleteAll(String query) {
        jdbcTemplate.getJdbcTemplate().update(query);
    }
}
