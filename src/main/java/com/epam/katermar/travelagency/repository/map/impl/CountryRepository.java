package com.epam.katermar.travelagency.repository.map.impl;

import com.epam.katermar.travelagency.domain.Country;
import com.epam.katermar.travelagency.repository.map.AbstractCrudRepository;

public class CountryRepository extends AbstractCrudRepository<Country> {
}
