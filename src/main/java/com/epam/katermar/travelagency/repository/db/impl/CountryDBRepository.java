package com.epam.katermar.travelagency.repository.db.impl;

import com.epam.katermar.travelagency.domain.Country;
import com.epam.katermar.travelagency.repository.CrudRepository;
import com.epam.katermar.travelagency.repository.db.AbstractDBRepository;

import java.util.List;
import java.util.Optional;

public class CountryDBRepository extends AbstractDBRepository<Country> implements CrudRepository<Country> {
    private static final String INSERT_INTO_COUNTRY_NAME_VALUES = "INSERT INTO country (name) VALUES (:name)";
    private static final String SELECT_FROM_COUNTRY_WHERE_ID = "SELECT id, name FROM country WHERE id = :id";
    private static final String DELETE_FROM_COUNTRY_WHERE_ID = "DELETE FROM country WHERE id = :id";
    private static final String SELECT_FROM_COUNTRY = "SELECT id, name FROM country";
    private static final String DELETE_FROM_COUNTRY = "DELETE FROM country";

    @Override
    public <S extends Country> S save(S entity) {
        return save(entity, INSERT_INTO_COUNTRY_NAME_VALUES);
    }

    @Override
    public Optional<Country> findById(Integer id) {
        return findById(id, SELECT_FROM_COUNTRY_WHERE_ID, (resultSet, rowNum) ->
                Optional.of(new Country(resultSet.getInt("id"), resultSet.getString("name"))));
    }

    @Override
    public List<Country> findAll() {
        return findAll(SELECT_FROM_COUNTRY, (resultSet, rowNum) ->
                new Country(resultSet.getInt("id"), resultSet.getString("name")));
    }

    @Override
    public void deleteById(Integer id) {
        deleteById(id, DELETE_FROM_COUNTRY_WHERE_ID);
    }

    @Override
    public void deleteAll() {
        deleteAll(DELETE_FROM_COUNTRY);
    }
}
