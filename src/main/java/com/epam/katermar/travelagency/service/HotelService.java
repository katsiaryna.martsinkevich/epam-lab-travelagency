package com.epam.katermar.travelagency.service;

import com.epam.katermar.travelagency.domain.Hotel;

/**
 * Created by katermar on 3/17/2018.
 */
public interface HotelService extends CrudService<Hotel> {
}
