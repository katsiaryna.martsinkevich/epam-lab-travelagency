package com.epam.katermar.travelagency.service;

import com.epam.katermar.travelagency.domain.Country;

/**
 * Created by katermar on 3/17/2018.
 */
public interface CountryService extends CrudService<Country> {
}
