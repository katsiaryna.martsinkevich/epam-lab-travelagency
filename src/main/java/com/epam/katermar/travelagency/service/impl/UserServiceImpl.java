package com.epam.katermar.travelagency.service.impl;

import com.epam.katermar.travelagency.domain.User;
import com.epam.katermar.travelagency.exception.NoDataException;
import com.epam.katermar.travelagency.repository.CrudRepository;
import com.epam.katermar.travelagency.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by katermar on 3/17/2018.
 */
public class UserServiceImpl implements UserService {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

    private CrudRepository<User> userRepository;

    @Override
    public <S extends User> S save(S entity) {
        LOGGER.debug("saving entity {} on a Service layer.", entity);
        return userRepository.save(entity);
    }

    @Override
    public User findById(Integer id) {
        LOGGER.debug("finding entity by id {} on a Service layer.", id);
        return userRepository.findById(id).orElseThrow(NoDataException::new);
    }

    @Override
    public List<User> findAll() {
        LOGGER.debug("finding all on a Service layer.");
        List<User> result = userRepository.findAll();
        if (result.isEmpty()) {
            throw new NoDataException();
        } else {
            return result;
        }
    }

    @Override
    public void deleteById(Integer id) {
        LOGGER.debug("deleting by id {} on a Service layer.", id);
        userRepository.deleteById(id);
    }

    @Override
    public void deleteAll() {
        LOGGER.debug("deleting all on a Service layer.");
        userRepository.deleteAll();
    }

    public void setUserRepository(CrudRepository<User> userRepository) {
        this.userRepository = userRepository;
    }
}