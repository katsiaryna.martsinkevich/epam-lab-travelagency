package com.epam.katermar.travelagency.service;

import com.epam.katermar.travelagency.repository.map.AbstractCrudRepository;

import java.util.List;

/**
 * Created by katermar on 3/17/2018.
 * <p>
 * Service delegates all methods of the {@link AbstractCrudRepository}
 *
 * @see AbstractCrudRepository
 */
interface CrudService<T> {
    /**
     * Saves a given entity. Use the returned instance for further operations as the save operation might have changed the
     * entity instance completely.
     *
     * @param entity must not be null.
     * @return the saved entity.
     */
    <S extends T> S save(S entity);

    /**
     * Retrieves an entity by its id.
     *
     * @param id must not be null.
     * @return the entity with the given id or Optional.empty() if none found
     */
    T findById(Integer id);


    /**
     * Returns all instances of the type.
     *
     * @return all entities
     */
    List<T> findAll();

    /**
     * Deletes the entity with the given id.
     *
     * @param id must not be null.
     */
    void deleteById(Integer id);


    /**
     * Deletes all entities managed by the repository.
     */
    void deleteAll();
}
