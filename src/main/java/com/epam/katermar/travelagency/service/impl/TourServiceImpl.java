package com.epam.katermar.travelagency.service.impl;

import com.epam.katermar.travelagency.domain.Tour;
import com.epam.katermar.travelagency.exception.NoDataException;
import com.epam.katermar.travelagency.repository.CrudRepository;
import com.epam.katermar.travelagency.service.TourService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by katermar on 3/17/2018.
 */
public class TourServiceImpl implements TourService {
    private static final Logger LOGGER = LoggerFactory.getLogger(TourServiceImpl.class);

    private CrudRepository<Tour> tourRepository;

    @Override
    public <S extends Tour> S save(S entity) {
        LOGGER.debug("saving entity {} on a Service layer.", entity);
        return tourRepository.save(entity);
    }

    @Override
    public Tour findById(Integer id) {
        LOGGER.debug("finding entity by id {} on a Service layer.", id);
        return tourRepository.findById(id).orElseThrow(NoDataException::new);
    }

    @Override
    public List<Tour> findAll() {
        LOGGER.debug("finding all on a Service layer.");
        List<Tour> result = tourRepository.findAll();
        if (result.isEmpty()) {
            throw new NoDataException();
        } else {
            return result;
        }
    }

    @Override
    public void deleteById(Integer id) {
        LOGGER.debug("deleting by id {} on a Service layer.", id);
        tourRepository.deleteById(id);
    }

    @Override
    public void deleteAll() {
        LOGGER.debug("deleting all on a Service layer.");
        tourRepository.deleteAll();
    }

    public void setTourRepository(CrudRepository<Tour> tourRepository) {
        this.tourRepository = tourRepository;
    }
}
