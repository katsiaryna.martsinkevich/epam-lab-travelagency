package com.epam.katermar.travelagency.service.impl;

import com.epam.katermar.travelagency.domain.Hotel;
import com.epam.katermar.travelagency.exception.NoDataException;
import com.epam.katermar.travelagency.repository.CrudRepository;
import com.epam.katermar.travelagency.service.HotelService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by katermar on 3/17/2018.
 */
public class HotelServiceImpl implements HotelService {
    private static final Logger LOGGER = LoggerFactory.getLogger(HotelServiceImpl.class);

    private CrudRepository<Hotel> hotelRepository;

    @Override
    public <S extends Hotel> S save(S entity) {
        LOGGER.debug("saving entity {} on a Service layer.", entity);
        return hotelRepository.save(entity);
    }

    @Override
    public Hotel findById(Integer id) {
        LOGGER.debug("finding entity by id {} on a Service layer.", id);
        return hotelRepository.findById(id).orElseThrow(NoDataException::new);
    }

    @Override
    public List<Hotel> findAll() {
        LOGGER.debug("finding all on a Service layer.");
        List<Hotel> result = hotelRepository.findAll();
        if (result.isEmpty()) {
            throw new NoDataException();
        } else {
            return result;
        }
    }

    @Override
    public void deleteById(Integer id) {
        LOGGER.debug("deleting by id {} on a Service layer.", id);
        hotelRepository.deleteById(id);
    }

    @Override
    public void deleteAll() {
        LOGGER.debug("deleting all on a Service layer.");
        hotelRepository.deleteAll();
    }

    public void setHotelRepository(CrudRepository<Hotel> hotelRepository) {
        this.hotelRepository = hotelRepository;
    }
}