package com.epam.katermar.travelagency.service.impl;

import com.epam.katermar.travelagency.domain.Country;
import com.epam.katermar.travelagency.exception.NoDataException;
import com.epam.katermar.travelagency.repository.CrudRepository;
import com.epam.katermar.travelagency.service.CountryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by katermar on 3/17/2018.
 */
public class CountryServiceImpl implements CountryService {
    private static final Logger LOGGER = LoggerFactory.getLogger(CountryServiceImpl.class);

    private CrudRepository<Country> countryRepository;

    @Override
    public <S extends Country> S save(S entity) {
        LOGGER.debug("saving entity {} on a Service layer.", entity);
        return countryRepository.save(entity);
    }

    @Override
    public Country findById(Integer id) {
        LOGGER.debug("finding entity by id {} on a Service layer.", id);
        return countryRepository.findById(id).orElseThrow(NoDataException::new);
    }

    @Override
    public List<Country> findAll() {
        LOGGER.debug("finding all on a Service layer.");
        List<Country> result = countryRepository.findAll();
        if (result.isEmpty()) {
            throw new NoDataException();
        } else {
            return result;
        }
    }

    @Override
    public void deleteById(Integer id) {
        LOGGER.debug("deleting by id {} on a Service layer.", id);
        countryRepository.deleteById(id);
    }

    @Override
    public void deleteAll() {
        LOGGER.debug("deleting all on a Service layer.");
        countryRepository.deleteAll();
    }

    public void setCountryRepository(CrudRepository<Country> countryRepository) {
        this.countryRepository = countryRepository;
    }
}
