package com.epam.katermar.travelagency.service.impl;

import com.epam.katermar.travelagency.domain.Review;
import com.epam.katermar.travelagency.exception.NoDataException;
import com.epam.katermar.travelagency.repository.CrudRepository;
import com.epam.katermar.travelagency.service.ReviewService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by katermar on 3/17/2018.
 */
public class ReviewServiceImpl implements ReviewService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ReviewServiceImpl.class);

    private CrudRepository<Review> reviewRepository;

    @Override
    public <S extends Review> S save(S entity) {
        LOGGER.debug("saving entity {} on a Service layer.", entity);
        return reviewRepository.save(entity);
    }

    @Override
    public Review findById(Integer id) {
        LOGGER.debug("finding entity by id {} on a Service layer.", id);
        return reviewRepository.findById(id).orElseThrow(NoDataException::new);
    }

    @Override
    public List<Review> findAll() {
        LOGGER.debug("finding all on a Service layer.");
        List<Review> result = reviewRepository.findAll();
        if (result.isEmpty()) {
            throw new NoDataException();
        } else {
            return result;
        }
    }

    @Override
    public void deleteById(Integer id) {
        LOGGER.debug("deleting by id {} on a Service layer.", id);
        reviewRepository.deleteById(id);
    }

    @Override
    public void deleteAll() {
        LOGGER.debug("deleting all on a Service layer.");
        reviewRepository.deleteAll();
    }

    public void setReviewRepository(CrudRepository<Review> reviewRepository) {
        this.reviewRepository = reviewRepository;
    }
}
