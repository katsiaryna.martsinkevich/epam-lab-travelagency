package com.epam.katermar.travelagency.service;

import com.epam.katermar.travelagency.domain.Tour;

/**
 * Created by katermar on 3/17/2018.
 */
public interface TourService extends CrudService<Tour> {
}
