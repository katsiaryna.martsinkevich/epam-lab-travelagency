INSERT INTO country (name)
VALUES ('Belarus');
INSERT INTO country (name)
VALUES ('USA');
INSERT INTO country (name)
VALUES ('Italy');
INSERT INTO country (name)
VALUES ('Turkey');
INSERT INTO country (name)
VALUES ('Egypt');
INSERT INTO country (name)
VALUES ('Israel');

INSERT INTO hotel (name, phone, id_country, stars)
VALUES ('Sunny Beach', '+78565486614', 4, 4);
INSERT INTO hotel (name, phone, id_country, stars)
VALUES ('Sunny Beach', '+78565487714', 5, 4);
INSERT INTO hotel (name, phone, id_country, stars)
VALUES ('Holy Apartments', '+48587486614', 6, 5);
INSERT INTO hotel (name, phone, id_country, stars)
VALUES ('Vetraz', '+375297869457', 1, 3);
INSERT INTO hotel (name, phone, id_country, stars)
VALUES ('Nesterka', '+375297869457', 1, 2);

INSERT INTO user (
  login, password)
VALUES ('elam', '123456');
INSERT INTO user (
  login, password)
VALUES ('kater', '123456');
INSERT INTO user (
  login, password)
VALUES ('lola', '123456');

INSERT INTO tour (
  photo, date, duration, id_country, id_hotel, description, cost, id_user, tour_type)
VALUES ('none', '2018-05-17', 'P7D', 4, 1, 'none', 124, 1, 'EDUCATION');
INSERT INTO tour (
  photo, date, duration, id_country, id_hotel, description, cost, id_user, tour_type)
VALUES ('none', '2018-05-15', 'P10D', 4, 1, 'none', 150, 2, 'EDUCATION');
INSERT INTO tour (
  photo, date, duration, id_country, id_hotel, description, cost, id_user, tour_type)
VALUES ('none', '2018-05-17', 'P10D', 5, 2, 'none', 150, 2, 'TRAVEL');

INSERT INTO review (
  content, id_user, id_tour)
VALUES ('good', 1, 1);
INSERT INTO review (
  content, id_user, id_tour)
VALUES ('bad service', 1, 3);
INSERT INTO review (
  content, id_user, id_tour)
VALUES ('good service', 3, 1);