SET DATABASE SQL SYNTAX PGS TRUE;

-- Type: TOUR_TYPE

-- DROP TYPE  TOUR_TYPE;

-- CREATE TYPE  TOUR_TYPE AS VARCHAR ('ECO', 'ADVENTURE', 'RURAL', 'MEDICAL', 'RELIGIOUS', 'EDUCATIONAL');

-- ALTER TYPE  TOUR_TYPE
--   OWNER TO postgres;

-- Table:  country

DROP TABLE IF EXISTS country CASCADE;

CREATE TABLE country
(
  id   SERIAL NOT NULL,
  name TEXT,
  CONSTRAINT country_pkey PRIMARY KEY (id),
  CONSTRAINT uq_country_name UNIQUE (name)
);

-- Table:  user

DROP TABLE IF EXISTS user CASCADE;

CREATE TABLE user
(
  id       SERIAL NOT NULL,
  login    TEXT,
  password TEXT,
  CONSTRAINT user_pkey PRIMARY KEY (id),
  CONSTRAINT uq_user_login UNIQUE (login)
);

-- Table:  hotel

DROP TABLE IF EXISTS hotel CASCADE;

CREATE TABLE hotel
(
  id         SERIAL NOT NULL,
  name       TEXT,
  phone      TEXT,
  id_country INTEGER,
  stars      INTEGER,
  CONSTRAINT hotel_pkey PRIMARY KEY (id),
  CONSTRAINT fk_hotel_country FOREIGN KEY (id_country)
  REFERENCES country (id) MATCH SIMPLE
  ON UPDATE CASCADE
  ON DELETE CASCADE
);

-- Table:  tour

DROP TABLE IF EXISTS tour CASCADE;

CREATE TABLE tour
(
  id          SERIAL NOT NULL,
  photo       TEXT,
  date        DATE,
  duration    TEXT,
  id_country  INTEGER,
  id_hotel    INTEGER,
  description TEXT,
  cost        NUMERIC,
  id_user     INTEGER,
  tour_type   TEXT,
  CONSTRAINT tour_pkey PRIMARY KEY (id),
  CONSTRAINT fk_tour_country FOREIGN KEY (id_country)
  REFERENCES country (id) MATCH SIMPLE
  ON UPDATE CASCADE
  ON DELETE CASCADE,
  CONSTRAINT fk_tour_hotel FOREIGN KEY (id_hotel)
  REFERENCES hotel (id) MATCH SIMPLE
  ON UPDATE CASCADE
  ON DELETE CASCADE,
  CONSTRAINT fk_tour_user FOREIGN KEY (id_user)
  REFERENCES hotel (id) MATCH SIMPLE
  ON UPDATE CASCADE
  ON DELETE CASCADE
);

-- Table:  review

DROP TABLE IF EXISTS review CASCADE;

CREATE TABLE review
(
  id      SERIAL NOT NULL,
  content TEXT,
  id_user INTEGER,
  id_tour INTEGER,
  CONSTRAINT review_pkey PRIMARY KEY (id),
  CONSTRAINT fk_review_tour FOREIGN KEY (id_tour)
  REFERENCES tour (id) MATCH SIMPLE
  ON UPDATE CASCADE
  ON DELETE CASCADE,
  CONSTRAINT fk_review_user FOREIGN KEY (id_user)
  REFERENCES user (id) MATCH SIMPLE
  ON UPDATE CASCADE
  ON DELETE CASCADE
);

